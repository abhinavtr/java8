package com.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogReader {

	public static void main(String args[]) {
		/*
		 * Assumption is logs are on daily basis and is in 00 hrs to 24 hrs 
		 * 
		 */
		String fileName = "fileToRead.txt";
		List<Integer> list = new ArrayList<>();
		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			list = stream
					.filter(line -> line.contains("logged in"))
					.map(line ->  line.substring(11,13))
					.map(line -> Integer.parseInt(line))
					.collect(Collectors.toList());
			
			Map<Integer, Long> items = list.stream()
				     .collect(Collectors.groupingBy(p -> p, 
				         Collectors.counting()));
			
			Optional<Entry<Integer, Long>> maxEntry = items.entrySet()
			        .stream()
			        .max((Entry<Integer, Long> e1, Entry<Integer, Long> e2) -> e1.getValue()
			            .compareTo(e2.getValue())
			        );
			    
			System.out.println(maxEntry.get().getValue() + " users logged in during hour "+ maxEntry.get().getKey() + " to "+ (maxEntry.get().getKey().intValue()+1));
	    
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
